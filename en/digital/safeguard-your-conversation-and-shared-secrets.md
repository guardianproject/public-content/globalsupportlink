# Safeguard your Conversation and Shared Secrets

1. Why do we need a secure communication tool?
2. What constitutes secure communication?
3. How to achieve Privacy and Anonymity?
4. What is Secure Data Sharing?

(This section introduces participants to the basic concept of encryption and presents them with tools for communication and options for data sharing that offer protection.)

**Secure Communication: Intro**

Secure communication is essential when two parties wish to converse without the risk of third-party interception or eavesdropping. This necessitates communication methods that are immune to such breaches of privacy.

1. &#x20;**Why do we need a secure communication tool?**

_**Your Device Can Listen to You:**_

* Your adversaries can potentially listen to your conversations by exploiting the built-in microphone in your phone and laptop/PC, particularly during regular insecure calls that are susceptible to interception.

_**Safeguarding Privacy and Identity:**_

* Secure communication tools are crucial for safeguarding privacy and identity, particularly when individuals or their contacts are at risk. By ensuring privacy and identity protection, the threat level can be reduced, enabling individuals and networks to continue their work without fear of compromise.

_**Prevention of Monitoring by Authorities:**_

* In situations where individuals are under constant surveillance by authorities, secure communication tools are essential for preventing access to the content of conversations, thereby maintaining privacy and confidentiality.

_**Keeping Platform Service Providers Out:**_

* Communication platform service providers may have access to all your conversations depending on whether the platform is encrypted or the level of encryption it provides.&#x20;
* For instance, platforms like WeChat, where the service provider has a direct tie to the regime, have exposed defenders in occupied regions like Tibet and East Turkestan to detention, arrest, and sentencing for sharing and receiving information via WeChat.

2. &#x20;**What constitutes secure communication?**

_**Data security when in transit**_

When selecting a communication platform, it's essential to consider whether they offer encryption and the level of protection it affords to your data during transit. There are two types of encryption:

* _What is HTTPS?_

HTTPS ensures the security of your data while it travels to its destination. Messages are encrypted in transit between the sender, service provider, and the receiver.

* _What is End-to-End Encryption?_

End-to-end encryption ensures that only the sender and receiver can view the messages. This means that only the devices involved in the communication process can encrypt and decrypt the messages. No intermediary, including the service provider, has the ability to decrypt and access the content of the messages. The end-to-end encryption provides the highest level of privacy and security.

3. &#x20;_**How to achieve Privacy and Anonymity?**_

Choosing the right communication tool is critical, especially for users under surveillance. Prioritize platforms that offer end-to-end encryption, ensuring only you and the recipient can access the message. Additionally, platforms offering anonymity provide an ideal level of security. It's also important to consider how the platforms handle your data, including what information they collect, how it's stored, and whether it's shared with third parties. Also to consider while selecting is who is behind the platform and who can have control over them. Additionally, for censored platforms, and for enhancing layers of security and privacy, you should use Tor or any trusted or reputable VPNs.

| <p><strong>Suggestion:</strong> (Depending on time available for the training)</p><ul><li>Include the following charts in the training presentation</li></ul><p>OR</p><ul><li>Write the charts on a flipchart before the training session,</li></ul><p>OR</p><ul><li>Ask participants to name popular communication platforms they are using, write them down on the chart, and inquire if the platform provides encryption and/or anonymity.</li></ul> |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

<mark style="color:red;">**Remember!**</mark> <mark style="color:red;"></mark><mark style="color:red;">If a platform does not provide any encryption, ignore it completely.</mark>\


**Secure Chat**

|        <p><br></p>       |                       **Platform**                      |                         **End-to-End Encryption**                        |                                                                      **Anonymity**                                                                     |
| :----------------------: | :-----------------------------------------------------: | :----------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------: |
| <p>Signal</p><p><br></p> |             App (desktop version available)             |                                     ✔                                    | <p>Somewhat - </p><p>Although a phone no; is required to use Signal, however, you can share just your username and choose to keep your no. private</p> |
|          Element         | App (desktop version available) and browser (web) based |                                     ✔                                    |                             <p>✔</p><p>Yes, if used with an account created without phone number or email registration.</p>                            |
|            Zom           |               App and browser (web) based               |                                     ✔                                    |                                  <p>✔</p><p>Does not require phone number or personal information for registration</p>                                 |
|         Whatsapp         | App (desktop version available) and browser (web) based |                                     ✔                                    |                                                    <p>✖</p><p>Requires phone number registration</p>                                                   |
|          Convene         |                   Browser (web) based                   | <p>.✔</p><p>End-to-End Encryption encrypted while using Private Mode</p> |                                                      <p>✔</p><p>Does not require registration</p>                                                      |



**Mobile Voice Call**\


| <p><br></p> |                       **Platform**                      | **End-to-End Encryption** |                                                    **Anonymity**                                                   |                                                                                  **Data Storage**                                                                                 |
| :---------: | :-----------------------------------------------------: | :-----------------------: | :----------------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|    Signal   |             App (desktop version available)             |             ✔             | <p>✔</p><p>Yes, if used with an account created without phone number or personal information for registration.</p> |                                                                             Does not collect any data                                                                             |
|   Whatsapp  | App (desktop version available) and browser (web) based |             ✔             |                                  <p>✖</p><p>Requires phone number registration</p>                                 | Does not store call logs on their server. Logs are stored locally on user devices. But metadata such as call duration, time of call, etc. are stored temporarily on their server. |
|    Viber    |             App (desktop version available)             |             ✔             |                                  <p>✖</p><p>Requires phone number registration</p>                                 | Does not store call logs on their server. Logs are stored locally on user devices. But metadata such as call duration, time of call, etc. are stored temporarily on their server. |
|    WeChat   | App (desktop version available) and browser (web) based |             ✖             |                                  <p>✖</p><p>Requires phone number registration</p>                                 |                                                      Does retain call and message data as well as user location information.                                                      |



**Video Call or Conference**\


| <p><br></p> |                       **Platform**                      | **End-to-End Encryption** |                                                    **Anonymity**                                                   |                                               **Data Storage**                                               |
| ----------- | :-----------------------------------------------------: | :-----------------------: | :----------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------: |
| Signal      |             App (desktop version available)             |             ✔             | <p>✔</p><p>Yes, if used with an account created without phone number or personal information for registration.</p> |                                           Does not collect any data                                          |
| Element     | App (desktop version available) and browser (web) based |             ✔             |           <p>✔</p><p>Yes, if used with an account created without phone number or email registration.</p>          | Does not store call logs but its infrastructure provider Matrix ip addresses and timestamps might be stored  |
| Jitsi       | App (desktop version available) and browser (web) based |             ✔             |                <p>✔</p><p>Does not require phone number or personal information for registration</p>               |                                   Does not retain user data on its servers.                                  |
| Google Meet |               App and browser (web) based               |             ✔             |                            <p>✖</p><p>Requires phone number registration</p><p><br></p>                            |                         Google stores data but it is encrypted in-transit and at rest                        |
| Whatsapp    | App (desktop version available) and browser (web) based |             ✔             |                                  <p>✖</p><p>Requires phone number registration</p>                                 |             Based on its privacy policy, Whatsapp does not store message contents once delivered.            |

4. **What is Secure Data Sharing?**

Just as maintaining high levels of privacy and anonymity is crucial for communication, it's equally important for sharing data securely. Prioritizing platforms with end-to-end encryption should be a top consideration. Additionally, there are various methods for sharing files, depending on the context and recipients, some of which involve sharing over the internet and others that do not.

_**Different methods for sharing file:**_

* Local Sharing
  * When both the sender and receiver are in close proximity, it's advisable to avoid using the internet for file sharing. Instead, utilize nearby features available on the device for sharing files directly. This method eliminates the need for data to travel over potentially insecure networks, enhancing security and privacy.
    * Bluetooth
    * Near Field Communication (NFC)
    * AirDrop
    * SD Cards, External Hard Drives
* App and Browser Based File Sharing Platforms

**Secure Data Sharing Tools**\


| <p><br></p>                                                                                         |             Platform            | End-to-End Encryption |                                                      Anonymity                                                     |                                                   Data Storage                                                  |
| --------------------------------------------------------------------------------------------------- | :-----------------------------: | :-------------------: | :----------------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------: |
| <p>Signal</p><p>(100 MB size limit)</p>                                                             |            App based            |           ✔           | <p>✔</p><p>Yes, if used with an account created without phone number or personal information for registration.</p> |                                             Does not store call logs                                            |
| Tresorit                                                                                            |   App and browser (web) based   |           ✔           |                    <p>✔</p><p>Yes, if used with an account created without a phone number. </p>                    |          <p>Does not store user encryption keys thus Tresorit cannot access user data. </p><p><br></p>          |
| Google Drive                                                                                        |   App and browser (web) based   |           ✔           |                                  <p>✖</p><p>Requires phone number registration</p>                                 | Google has access to all data stored but not authorized users as they are all encrypted in-transit and at-rest. |
| <p>Nextcloud</p><p>(Groups can host their own nextcloud server and have more control over data.</p> |   App and browser (web) based   |           ✔           |               <p>✔</p><p>Does not require phone number or personal information for registration.</p>               |           Since it’s a self-hosted file sharing platform, it does not retain user data on its server.           |
| OnionShare                                                                                          | App (desktop version available) |           ✔           |                                                          ✔                                                         |                             Does not retain data shared through onion or user data.                             |

