# Better Living through Threat Modeling!

| <p><strong>Explanation:</strong> </p><p>Normally, we conduct threat modeling training at the beginning, allowing participants to continuously update their Assets, Risks, Vulnerabilities, and Mitigation as they recognize them throughout the training. This approach is suitable for 2-3 day training sessions. However, for shorter sessions, we've found that conducting threat modeling at the end, after participants have comprehended all the mitigation steps, proves to be more effective.</p> |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

As we explore digital security practices to safeguard against potential cyber threats, it's essential to recognize that cybersecurity is highly individualized. What may work for one person may not be effective for others, given their unique circumstances. Factors such as personal objectives, associated risks, and mitigation strategies all come into play.

In the ever-evolving landscape of digital security, continual updates and adaptability are crucial. To facilitate this understanding, we'll delve into a condensed version of the Threat Modeling exercise. This exercise serves as a valuable framework for evaluating risks and implementing protective measures.

**Instruction:**

* Divide the participants into 3 groups and assign each group a scenario. Add more scenarios (and groups) based on the number of participants or regional needs and experiences.
* During the exercise, each group will address their assigned scenario and work together to devise solutions based on the content covered in modules 1, 2, and 3. Participants are also encouraged to draw from their own experiences and include additional solutions not covered in the modules. Real-life experiences should be taken into consideration when formulating solutions. The answers to the following questions will lay the groundwork for their individual threat models, shaping their approach to digital security:
  * What assets do I/we aim to protect?
  * Who poses potential threats to these assets?
  * What are the consequences of a security breach?
  * How likely is it that I/we will need to protect these assets?
  * What steps would I/we and I/our colleagues take to protect these assets?

**Suggestion:**

The scenarios for this exercise can be based on the real-life experiences and insights of the participants and their region. Drawing from actual scenarios will help generate more authentic mitigation strategies.



_**Scenario 1:**_

You and your team of defenders are organizing a significant public event in five months to protest a government policy that undermines basic human rights. Anticipating the government's relentless efforts to impede your organization of this event, heightened vigilance and strategic planning are imperative.

In a week, you have your inaugural meeting with the entire organizing team, comprising defenders and partners with varying risk levels - low, medium, and high. During this session, you'll discuss confidential information and make pivotal decisions regarding event planning and your team of organizers will have to continue communication for the next five months.

_**Scenario 2:**_

It's already been 5 months, and there are only two days left until the big event. Congratulations on making it this far! Despite facing intimidation from the authorities over the past five months, you and your team have persevered. However, there's concerning news: word has spread that the government plans to bring in police forces from neighboring states or provinces as backup and intends to crack down on the event. As an organizer participating in the event, there's a high likelihood of being arrested either before or during the event.

_**Scenario 3:**_

You are collaborating with a highly targeted community in \[Name of Remote Region], which is resisting mining supported by the local government. The local authority and the mining company persistently intimidate community leaders and threaten to harm or arrest them if they're discovered scheming against the government.

Your role involves assisting the community in collecting all evidence related to the mining operation, which could include proof of corruption, pollution, adverse effects on the health and well-being of the local population and wildlife, and other pertinent issues. However, you're only able to visit the region once every three months, and most of your communication with the community occurs via your mobile phone.
