# You are the Best Antivirus

1. Strong Password
2. Do the 2-Step!
3. Detach from Attachments!
4. Think Before You Click!
5. Don’t Be a Phish!
6. Don’t Wait, Update!
7. Browser Security
8. Don’t Be a Hoarder
9. Don’t Share Drives
10. Be a 30-sec Detective
11. Keep Your Data Under Wraps

This section aims to introduce or remind participants of the best practices to maintain good digital security hygiene.

**Digital Security Best Practices: Intro**

Having antivirus software running on your device offers an additional layer of security; however, it's crucial to recognize that antivirus software may not always suffice, particularly in the face of zero-day attacks. Your first line of defense should always be yourself. By adhering to straightforward daily best practices, you can effectively thwart potential adversaries.&#x20;

{% embed url="https://www.youtube.com/watch?list=PL0Hcq8UCiYqlKxhKpH_K4P1m-HFSjdu7w&v=CztQyVYiXQs" %}
Be A Cyber Superhero! By Tibet Action Institute
{% endembed %}

1. &#x20;**Strong Password**

It's often tempting to create simple, easy-to-memorize passwords for all the platforms you need access to, simply because there are so many to remember. However, this approach can inadvertently grant adversaries and cybercriminals easy access to all your accounts.

* Avoid using passwords that can be easily guessed, such as your name, date of birth, important dates, family members' names, birthplace, etc.
* Instead, passwords should be complex and incorporate a mix of uppercase and lowercase letters, numbers, and symbols. Utilizing multiple characters makes it significantly more challenging for anyone to guess.
  * Here's an example of how to create a complex yet memorable password::
    * Construct a sentence that is easy to remember. For example: "My list of favorite activities definitely doesn't involve jogging at six in the morning!"
    * Use the initials of each word in the sentence, including capital letters, and replace words with numbers or symbols where possible. For instance: "Mlofaddij@6itm!"
* Remembering numerous sentences for different accounts can be challenging. To simplify this process, consider using password managers like Bitwarden, which fully encrypts data, ensuring that even the creators of the tool cannot access your data. With a password manager, you only need to remember one master password to access Bitwarden, where you can securely store passwords for all your accounts.

2. &#x20;**Do the 2-Step!**

Now that you've strengthened your password, you've bolstered your account security. However, attackers may still find other avenues to gain unauthorized access. To add an extra layer of protection, consider enabling two-step authentication or verification if the platform offers this feature. Instead of relying on SMS verification, which can be intercepted, opt for a trusted authenticator app. By doing so, even if an attacker manages to crack your password, they'll still be unable to access your account without the second verification step.

* Additionally, if your device is at risk of theft or confiscation, it's essential to save backup codes. These codes will serve as a lifeline, allowing you to regain access to your account if you lose access to your authentication app.

3. &#x20;**Detach from Attachments!**



{% embed url="https://www.youtube.com/watch?index=6&list=PL0Hcq8UCiYqn7PhgxpY2uiA4QkGVj_V1c&v=v4E1SRDmtZE" %}
Detach from Attachments! By Tibet Action Institute
{% endembed %}

In Buddhism, Attachment is considered the root of all suffering, and this principle holds true in digital security as well. Adversaries often seek access to your systems to gain control and manipulate your devices and accounts, thereby obtaining complete access to your data. One of the most popular ways for attackers to achieve their goals is by enticing their targets to open malicious attachments.&#x20;

Most targeted attacks are socially engineered, often leveraging emails or messages on platforms like WhatsApp containing content highly relevant to you or your group. These messages exploit our interests and concerns, making them more convincing. We must know that even in high-profile compromises within the government, vulnerabilities can stem from individual mistakes made by employees, like downloading and opening a malicious attachment, despite robust cyber defense measures in place.

Malware could provide intruders with access to your network, files, camera, microphone, keyboard log, and other sensitive information as soon as you download and open it. To prevent compromise, it's crucial to detach from attachments. Instead of sending attachments, prioritize using encrypted shareable drives like Google Drive, Tresorit, etc., to securely share files with your contacts or network.&#x20;

If you receive an attachment that appears genuine, refrain from downloading and opening it immediately. Instead, verify its legitimacy with the sender through another platform before taking any further action.&#x20;

4. &#x20;**Think Before You Click!**

Sending malicious links is another tactic adversaries use to attack you. Similar to malicious attachments, adversaries may craft personalized messages or emails to entice you into clicking on these links, potentially compromising your network, personal data, or organizational information, or taking control of your device. In targeted attacks, the links displayed in the message, especially in emails, may not lead to the destinations they appear to. Here are some tips on how to handle such situations:

* Do not click on links in emails or messages unless absolutely necessary. If you feel compelled to open a link, verify its legitimacy with the sender through another platform before clicking it.
* If you're using a laptop or PC, hover your mouse over the link and check if the link that appears matches the one in the email. Be aware that adversaries often create links very similar to those of legitimate companies, organizations, or news agencies to deceive recipients. Examine the link carefully for any discrepancies. For example, if the link appears to lead to google.com, it may actually be go0gle.com or goo9le.com, or google-com.com, etc.

5. &#x20;**Don’t Be a Phish!**

{% embed url="https://www.youtube.com/watch?index=3&list=PL0Hcq8UCiYqkUVnpduzynZw9dpGRcqNhs&v=gNRzivSBonQ" %}
Don't Be  Phish! By Tibet Action Institute
{% endembed %}

Phishing is another tactic adversaries could employ to steal your personal information, such as passwords or bank login details. Phishing attacks can vary in scale, from targeted attempts to widespread campaigns, depending on the motive.

In such attacks, particularly when targeted, the perpetrators may impersonate individuals or groups with whom you have affiliations or whom you would naturally trust, such as partner organizations or funders. They may also pose as reputable organizations like Google or banks (names varying by region) and send emails enticing you to enter your personal information into a fake login page.

* Never enter your username and password if you receive an email or message requesting such information.&#x20;
* While organizations, banks, and email service providers may send notifications in case of a breach, attackers exploit this by attempting to deceive you. If you're unsure about the legitimacy of an email, contact the organization or bank directly to confirm its authenticity.
* Additionally, for your email and other communication platforms, enable 2-step verification as an added layer of security.

6. &#x20;**Don’t Wait, Update!**

Often, when individuals receive notices on their computers prompting them to update software with options like 'Update Now' or 'Remind Me Later,' they might opt to postpone the update due to being engaged in other tasks. However, updates not only introduce new and improved features but also crucially provide security patches for any vulnerabilities detected in previous versions.

* Opting to 'Update Now' is a simple yet powerful way to enhance online safety. This is because many malicious attacks exploit vulnerabilities found in older versions of operating systems, software, and apps.

7. &#x20;**Browser Security**

Browsers serve as the gateway to the internet, facilitating access to a wealth of information. However, they also store significant amounts of personal data, including browsing history, user credentials, and banking information. Moreover, accessing malicious sites can expose users to various threats, serving as entry points for attackers. Therefore, adopting good practices around browser security is essential to defend against potential threats. Here are a few steps you can take to ensure secure browsing:

_**Choose the Right Browser:**_

* Choose browsers that prioritize security. Options like Chrome, Brave, Firefox, and others are known for their focus on privacy and security features.

_**Keep your Browser Up-to-Date**_

* Regularly updating your browser is crucial for security. Many attacks target vulnerabilities found in older versions, so staying up-to-date helps keep attackers at bay.

_**Use add-ons/extensions to enhance browser security:**_

* Add-ons like uBlock Origin, NoScript, and Privacy Badger help block malicious scripts and trackers, reducing the risk of compromise while browsing.
* HTTPS Everywhere enforces HTTPS encryption whenever possible, enhancing privacy and security whenever possible and provide privacy while browsing&#x20;

_**Use Incognito/Private Browser:**_

* When browsing sensitive websites or conducting private activities, utilize the Incognito or Private Browsing mode available in most browsers. This mode automatically deletes browsing history and cookies when the session is closed, minimizing the risk of exposure.

_**Delete Browser History:**_

* Regularly clear your browsing history to remove any traces of your online activity. This helps protect your privacy and prevents unauthorized access to your browsing habits.

_**Do not save personal information:**_

* Avoid saving login credentials, banking details, or other sensitive information in your browser. While it may seem convenient, storing this information makes it easily accessible to anyone who gains access to your device.

8. &#x20;**Don’t Be a Hoarder**

{% embed url="https://www.youtube.com/watch?index=1&list=PL0Hcq8UCiYqkUVnpduzynZw9dpGRcqNhs&v=xLMH4DK-Y1I" %}
Don't be a hoarder! By Tibet Action Institute
{% endembed %}

In our fast-paced digital world, it's easy to accumulate a clutter of personal and work-related information in our digital closets. These may include emails, chat conversations, photos, and files, posing potential risks not only to ourselves but also to our families and networks, depending on the sensitivity of the content. While it's challenging to find time for digital cleanup amidst our busy schedules, dedicating a specific time annually to deep clean can be immensely beneficial. Not only does it safeguard us against future threats, but it also enhances device performance. Here are a few tips to avoid digital hoarding:

* Regularly delete sensitive communications or chats from both ends once the conversation is concluded.
* Delete media and other files promptly after use if they serve no further purpose.

9. &#x20; **Don’t Share Drives**

Thumb drives may seem like relics to many, but external storage devices remain crucial for data backup and offline storage. However, it's important to exercise caution when using these drives. Avoid sharing or connecting them to devices that are not your own. If the other device is compromised, it could pose a risk to the data stored on your drive as well as your laptop or PC.

* Use file sharing platforms such as Google Drive, Tresorit, or similar services to securely share drives online.
* If online sharing is not feasible, consider using features like AirDrop, Bluetooth, or other nearby sharing options to transfer data securely between devices.

10. &#x20;**Be a 30-sec Detective**

In the realm of digital communication, much like a traditional letter, an email bears crucial information about its origin and destination. Even when the sender is familiar, it's imperative to verify the authenticity of emails containing links or attachments. Dedicate at least 30 seconds to scrutinize for potential red flags, including:

* Spelling discrepancies in the sender's name (e.g., "Callvin" instead of "Calvin").
* Recognizable senders using unfamiliar domains (e.g., "calvin@yahoo.com" or “calvin@gmaiil.com” instead of "calvin@gmail.com") can be a red flag for potential phishing attempts.
* Presence of links or attachments within the email.
* Generic or impersonal content in the email body.

By remaining vigilant and identifying these warning signs, you can safeguard yourself against potential phishing attempts or fraudulent activity.

11. &#x20;**Keep Your Data Under Wraps**

If you reside in an area with significant censorship and surveillance, if you're under active monitoring, incorporating Tor or trusted or reputable VPNs into your digital security practices whenever feasible is crucial. This is especially important when transmitting or accessing sensitive information.

* VPN - a trusted or good VPN will provide:
  * **Privacy**
  * Accessibility&#x20;
  * Security
  * Ex.:Psiphon, Lantern
* Tor provides:
  * **Privacy**
  * Accessibility
  * Security
  * **Anonymity**
  * Ex.: Tor browser, Orbot\
