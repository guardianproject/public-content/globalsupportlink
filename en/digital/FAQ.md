**How do I know if my device is compromised?**

Here are some signs that may indicate something is wrong with your device:

1. Battery and Performance Issues: Sudden and rapid depletion of battery life, unusual heating of the device, and noticeable slowing down of device performance, or sudden shutdowns
2. Missing Files: Files mysteriously disappearing.
3. Unexpected Pop-ups: Unexpected windows and screens popping up.
4. Unauthorized Installations: Unauthorized apps or software installed on your device.
5. Suspicious Account Activity: Your email or social media accounts logging in from different locations.
6. Active Camera and Microphone: Your device camera and microphone are active when you are not using them.


Some devices may not display obvious signs of compromise. If you suspect your device is compromised and do not have the resources to remove the malware, the best course of action is to back up your files and perform a factory reset.

**How do I know if my email is compromised?**

Here are some signs that may indicate that your email may be compromised:

1. Unable to Log In: You are unable to log into your email account using your credentials.
2. Unusual Emails Sent: Your friends start receiving random emails from you, or your sent folder contains emails you never sent.
3. Unauthorized Notifications: You start receiving notifications of login attempts or password changes without your action.
4. Account Details Changed: Details in your account have changed, including your phone number, recovery email, mail forwarding address, etc.
5. Suspicious Account Activity: Your account activity displays IP address locations unrelated to you, logins occurring at unusual hours, or devices and browsers that you do not typically use.

If your email is exhibiting any of these signs:
1. Immediately change your password.
2. From your account activity, log out devices/locations that are not familiar to you.
3. Set up 2-Step Verification.
4. Inform all contacts who received malicious emails from your account.

To avoid being compromised:
1. Use strong passwords
2. Refrain from downloading and opening attachments
3. Email service providers never send emails asking you to enter your username and password in forms embedded in an email or through links. If you receive such an email, do not enter your credentials. This could be a phishing attack. If you genuinely need to reset your password, you should request it through the official website where you will receive a link to reset it securely.
4. Always enable 2-Step Verification for added security
5. Regularly update your email application, browser, and device operating system (OS) to patch security vulnerabilities
6. Verify the sender's email address if an email appears suspicious and contains attachments or links


**How do I know if my social media account is compromised?**

Here are some signs that may indicate that your social media may be compromised:
1. Unable to Log In: You are unable to log into your email account using your credentials.
2. Unusual Posts: Random content starts appearing on your account that you did not post
3. Unusual Messages: Your friends/contacts start receiving messages, links and attachments that you did not send
4. nauthorized Notifications: You start receiving notifications of login attempts or password changes without your action.
5. Account Details Changed: Details in your account have changed, including details in your bio, profile picture, phone number connected with the account, recovery email, linked accounts, etc.
6. Suspicious Account Activity: Your account activity displays IP address locations unrelated to you, logins occurring at unusual hours, or devices and browsers that you do not typically use

If your account is exhibiting any of these signs:
1. Immediately change your password.
2. Log out devices/locations that are not familiar to you.
3. Set up 2-Step Verification.
4. Inform all contacts who received malicious emails from your account.

To avoid being compromised:
1. Use strong passwords
2. Refrain from downloading and opening attachments
3. Avoid clicking on suspicious links in posts and messages
4. Platform service providers never send emails asking you to enter your username and password in forms embedded in an email or through links. If you receive such an email, do not enter your credentials. This could be a phishing attack. If you genuinely need to reset your password, you should request it through the official website where you will receive a link to reset it securely.
5. Always enable 2-Step Verification for added security
6. Regularly update your social media application, browser, and device operating system (OS) to patch security vulnerabilities


**How do I know if my phone is being tapped?**

The scope of tapped devices has expanded significantly beyond traditional methods and now includes surveillance through communication apps you use.

The most common signs of being tapped during regular phone calls:
Background noises such as clicking, echoes, or voice feedback
Call disruptions like unexpected drops when neither party hangs up

Regular phone calls and messages can be intercepted using surveillance equipment.
If your communication device is compromised, you can also be monitored when using communication apps. Here are some signs that may indicate something is wrong with your device:
1. Sudden and rapid depletion of battery life, unusual heating of the device, and noticeable slowing down of device performance, or sudden shutdowns
2. Files mysteriously disappearing.
3. Unexpected windows and screens popping up.
4. Unauthorized apps or software installed on your device
5. Your email or social media accounts logging in from different locations.
6. Your device camera and microphone are active when you are not using them.
7. Some devices may not display obvious signs of compromise. If you suspect your device is compromised and do not have the resources to remove the malware, the best course of action is to back up your files and perform a factory reset.
To avoid being tapped:
Always use communication apps that provide End-to-End Encryption (E2EE) like Signal and WhatsApp
Follow digital security best practices to keep your device and account safe https://guide.globalsupport.link/digital/you-are-the-best-antivirus

**How can I create a strong password?**

It's often tempting to create simple, easy-to-memorize passwords for all the platforms you need access to, simply because there are so many to remember. However, this approach can inadvertently grant adversaries and cybercriminals easy access to all your accounts.
Avoid using passwords that can be easily guessed, such as your name, date of birth, important dates, family members' names, birthplace, etc.
Instead, passwords should be complex and incorporate a mix of uppercase and lowercase letters, numbers, and symbols. Utilizing multiple characters makes it significantly more challenging for anyone to guess.
Here's an example of how to create a complex yet memorable password::
Construct a sentence that is easy to remember. For example: "My list of favorite activities definitely doesn't involve jogging at six in the morning!"
Use the initials of each word in the sentence, including capital letters, and replace words with numbers or symbols where possible. For instance: "Mlofaddij@6itm!"
Remembering numerous sentences for different accounts can be challenging. To simplify this process, consider using password managers like Bitwarden, which fully encrypts data, ensuring that even the creators of the tool cannot access your data. With a password manager, you only need to remember one master password to access Bitwarden, where you can securely store passwords for all your accounts.

**Is it possible to have secure communication?**

Yes, secure communication is possible with End-to-End Encryption (E2EE). For more information, please visit: https://guide.globalsupport.link/digital/safeguard-your-conversation-and-shared-secrets 

Is it safe to have conversation over regular SMS and phone calls?
Regular phone calls and messages can be intercepted using surveillance equipment.

For secure communication:
Always use communication apps that provide End-to-End Encryption (E2EE) like Signal and WhatsApp
Follow digital security best practices to keep your device and account safe https://guide.globalsupport.link/digital/you-are-the-best-antivirus 


**What is the best secure messaging app?**

There are few criterias we must consider when choosing a secure app.
* Whether the app provides End-to-End Encryption (E2EE)
* If the app ensure Anonymity
* If the app developer or service provider has a history of association with or working for agencies that are not supportive of you or your work.

For a list of messaging apps, please refer to the 'Secure Chat' chart on this page:https://guide.globalsupport.link/digital/safeguard-your-conversation-and-shared-secrets

**How can I secure my email?**

Here are few steps you can take to secure your email:
1. Use strong passwords
2. Refrain from sending attachments or downloading and opening attachments received
3. Email service providers never send emails asking you to enter your username and password in forms embedded in an email or through links. If you receive such an email, do not enter your credentials. This could be a phishing attack. If you genuinely need to reset your password, you should request it through the official website where you will receive a link to reset it securely.
4. Always enable 2-Step Verification for added security
5. Regularly update your email application, browser, and device operating system (OS) to patch security vulnerabilities
6. Verify the sender's email address if an email appears suspicious and contains attachments or links

Implement the best digital security practices outlined on this page: https://guide.globalsupport.link/digital/you-are-the-best-antivirus


**Does VPN hide all my personal and browsing information?**

Although VPNs have the ability to encrypt personal and browsing information of users when connecting to the internet, VPNs do not provide complete anonymity. If you wish to have complete anonymity while accessing the internet, the best circumvention tool is to Tor: https://www.torproject.org/

**Which browser is the most secure?**

Go for browsers that are not built by or are associated with agencies that are not supportive of you or your work. Here are some of the more popular secure browsers: Tor Browser, Brave, Firefox, Chrome and Safari.

Securing your browsing experience also requires implementing best browser security practices:

Choose the Right Browser:
* Choose browsers that prioritize security. Options like Chrome, Brave, Firefox, and others are known for their focus on privacy and security features.
* Keep your Browser Up-to-Date
* Regularly updating your browser is crucial for security. Many attacks target vulnerabilities found in older versions, so staying up-to-date helps keep attackers at bay.
* Use add-ons/extensions to enhance browser security:
* Add-ons like uBlock Origin, NoScript, and Privacy Badger help block malicious scripts and trackers, reducing the risk of compromise while browsing.
* HTTPS Everywhere enforces HTTPS encryption whenever possible, enhancing privacy and security whenever possible and provide privacy while browsing
* Use Incognito/Private Browser:
* When browsing sensitive websites or conducting private activities, utilize the Incognito or Private Browsing mode available in most browsers. This mode automatically deletes browsing history and cookies when the session is closed, minimizing the risk of exposure.
* Delete Browser History:
* Regularly clear your browsing history to remove any traces of your online activity. This helps protect your privacy and prevents unauthorized access to your browsing habits.
* Do not save personal information:
Avoid saving login credentials, banking details, or other sensitive information in your browser. While it may seem convenient, storing this information makes it easily accessible to anyone who gains access to your device.

**How do I secure my laptop?**
	
Here are steps you can take to secure your laptop:
1. Lock your laptop using Strong Passwords or Passphrases. Remember, while Face ID, Biometrics, simple Patterns, and 4-digit PINs are convenient, they are not the most secure options. These methods can be easily cracked, or you may be coerced into unlocking the device, especially if you live under repressive regimes.
2. Encrypt your device's hard drive using the tools available on your laptop. For Windows laptops, you can use BitLocker, and for macOS, you can use FileVault. This will help protect the data on your device.
3. Do not use public WiFi (at airports, libraries, cafes, etc.) unless absolutely necessary. If you must use public WiFi, ensure you are using Tor or a VPN to connect to the internet.
4. Update your Operating System (OS) and apps regularly. Do not delay updates.
5. Protect your device from attackers by detaching with attachments, thinking before clicking on links, and avoiding phishing attempts. If an email or message looks suspicious, do not engage. If it appears legitimate but still raises doubts, confirm with the sender through another channel.
6. Remember, you are the best antivirus. Implement the digital security best practices outlined here to secure your device and accounts: https://guide.globalsupport.link/digital/you-are-the-best-antivirus.
Consider the physical security of your device as well. Do not leave it unattended, especially in public spaces. 

**How do I secure my phone?**

Here are steps you can take to secure your laptop:
1. Don't Let Anyone Into Your Device. Use a passphrase to unlock your phone. Remember, while Face ID, Biometrics, simple Patterns, and 4-digit PINs are convenient, they are not the most secure options. These methods can be easily cracked, or you may be coerced into unlocking the device, especially if you live under repressive regimes.
2. Encrypt your mobile devices. Make sure encryption is enabled on your Android or iOS devices. This will help protect the data on your device.
3. Don’t let bad apps crash your party. Only install apps that you need, as more apps increase the potential for surveillance. Ensure the apps you use are only accessing the data and device features necessary for them to function properly.
4. Do not use public WiFi (at airports, libraries, cafes, etc.) unless absolutely necessary. If you must use public WiFi, ensure you are using Tor or a VPN to connect to the internet.
5. Update your Operating System (OS) and apps regularly. Do not delay updates.
6. Protect your device from attackers by detaching with attachments, thinking before clicking on links, and avoiding phishing attempts. If an email or message looks suspicious, do not engage. If it appears legitimate but still raises doubts, confirm with the sender through another channel.
7. Enable ‘Find My Phone’ for locating your device if it's lost or misplaced. Some features in Find My Phone can be valuable for defenders, especially when your device is confiscated or falls into the hands of adversaries. Of course, this feature 

**Here's how you can enable Find My Phone:**
iPhone: https://support.apple.com/en-ca/102648
Android: https://support.google.com/accounts/answer/3265955?hl=en
When your device is confiscated or in the possession of adversaries, consider the following actions based on your situation:
1. Log in to android.com/find or icloud.com/find depending on what device you are using.
2. Remotely set up a device PIN number if you haven't already done so. This can delay others' access to your device.
3. Remotely erase data on your device if you have any information that could compromise your safety and the safety of your network.
4. Remember, you are the best antivirus. Implement the digital security best practices outlined here to secure your device and accounts: https://guide.globalsupport.link/digital/you-are-the-best-antivirus
5. Consider the physical security of your device as well. Do not leave it unattended, especially in public spaces.

**What should I do to prevent apps from tracking my location?**
		
Here are steps you can take to prevent apps from tracking your location:
1. Turn off location service at all times. Only enable them when you need to use a location-based feature.
2. Do not allow apps from accessing your location. Only grant location access when it is required for the app to function properly. Go to device Settings and revoke permissions from App permissions (for Android) and Privacy (for iOS).
3. Restrict apps, especially social media apps, from tracking your location and collecting data. Most apps have options to limit access within the app's privacy settings.
4. Restrict apps from Ads Personalization or Apple Advertising through your device’s Privacy Settings.
5. Use Tor or VPN to hide your actual location.
	
