---
description: Powered by the People and Ready to Assist!
---

# Welcome to Your Help Desk

_Ahora disponible en Español / Castellano en_ [_https://ayuda.globalsupport.link/_](https://ayuda.globalsupport.link/)

The Help Desk provides **free** resources to people and organizations who are part of the **Powered by the People (PxP)** global network. In addition to the online resources, members of the [Help Desk](ask-for-help.md) can answer questions and provide advice on digital and physical security via chat, messaging or email channels.&#x20;

_**PLEASE NOTE: THE HELP DESK TEAM IS CURRENTLY PROVIDING ALL SUPPORT PRO-BONO AND WILL RESPOND AS BEST AS POSSIBLE TO REQUESTS**_

**Reach out today to discuss with a member of the** [**Help Desk**](ask-for-help.md) to see what is right for you or your organization or click through the resources available online:

* [Digital Security](digital/)
* [Physical Security](physical/)
* [Psychosocial Support](psychosocial.md)

This is an evolving resource that will be updated and improved with feedback from all of you!
