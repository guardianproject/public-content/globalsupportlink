---
description: Straightforward steps you can take to protect your mobile devices
---

# Device Security: Preparing for Theft, Loss or Confiscation

Whether crossing an international border or checkpoint, attending a demonstration, or walking around an unfamiliar city, our devices, especially our mobile phones, are always at risk of being stolen, misplaced or confiscated.&#x20;

Before traveling or attending an event that could lead to your detention or arrest, back up as much as possible from your devices to cloud storage or your organization’s server, including passwords, messaging conversations, and documents.

**Documents & Files**

* Delete any sensitive files from the device or move them to an encrypted volume.
* Clear all content from Downloads, Pictures, Music and Video folders.
* Empty the recycle bin.

**Encryption Software & Messaging**&#x20;

* Uninstall encryption software, and only reinstall once destination is reached.
* Once conversations are backed up, you should log out of and uninstall any sensitive messaging applications.
* If you need to keep a messaging application for communication purposes, set disappearing messages for as short a time as possible.
* Delete any email applications and rely on webmail as much as possible.

**Web Browsers**

* Log out of all websites and services.
* Clear temporary internet files and browsing history.

**Virtual Private Networks (VPN)**

* VPN logs and profiles should be cleared.

**Windows Temporary Files and Caches**

* Click on Start, and then type ‘Disk Clean-up’ into the search Programs and Files Bar.
* Choose Drive C: from the drop down list and press OK.
* Tick all options in the ‘Files to Delete’ box and press OK.
