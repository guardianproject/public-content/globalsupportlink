---
description: All the ways you can reach out to ask for help
---

# ☎️ Ask for Help!

**This service is currently being operated pro-bono as our funding is "on pause".** Currently, we can provide limited advice and technical support on topics related to digital security, and in some cases we may refer you to a regional partner or organisation that is better placed to assist.

**OUR PARTNER HELP DESKS REMAIN FULLY ACTIVE AND CAN BE CONTACTED THROUGH THE INFORMATION BELOW:**

The [Miaan help desk](https://miaan.org/) can be reached through the contact channels below:

Email[^1]: [helpdesk@miaan.org](mailto:helpdesk@miaan.org)

Signal or WhatsApp: +46 766 860 503

Telegram: @miaan\_helpline\_bot

\
The [Article 19 help desk](https://www.article19.org/) can be reached through the contact channels below:

Email: [Escalations@article19.org](mailto:Escalations@article19.org)

Signal, WhatsApp or Telegram: +44 73 4000 1385



Otherwise, you can still contact us at:

* Public Help Desk Chat: [chat.globalsupport.link](https://chat.globalsupport.link)
*   Help Desk Support:

    * Email: [help@globalsupport.link](mailto:help@globalsupport.link)
    * Signal or WhatsApp: [+447886176827](https://wa.me/+447886176827)
    * Telegram: [@GlobalSupportLink\_bot](https://t.me/GlobalSupportLink_bot)



**Note:** We are not in a position to provide visa support, medical bills assistance, organizational fees (such as start-up costs), general humanitarian aid, extensive legal support, general funding requests, and we do not currently conduct advocacy activities.&#x20;

_This help desk is powered by_ [_free, open-source, secure and audited software_](https://digiresilience.org/solutions/link/)_, and hosted in a privacy-focused, environmentally concerned, and physically controlled_ [_datacenter_](https://greenhost.net/)_._

[^1]: 
