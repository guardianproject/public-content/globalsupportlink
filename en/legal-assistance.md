---
description: Equip yourself with knowledge to better understand and navigate legal risks.
---

# 📜 Legal Assistance

Ask us about:

* The content of laws, regulations, and other legal instruments that affect the rights of activists, movements, and organizations. For example: protest regulations, cybercrime laws, foreign influence laws, access to information laws, laws on organizational registration, counterterrorism and other laws that authorities may use to restrict activism and punish dissent.
* International standards and legal arguments that support the rights of activists, movements, and organizations
* Comparative examples and good practices in laws affecting the rights of activists, movements, and organizaitons
* Research on effective strategies, activists, movements, and organizations can use to navigate restrictive laws and advocate for a more enabling legal environment.
