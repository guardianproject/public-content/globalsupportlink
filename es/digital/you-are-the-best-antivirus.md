you-are-the-best-antivirus.md

Eres el mejor antivirus

Contraseña segura
¡Haz los 2 pasos!
¡Sepárese de los archivos adjuntos!
¡Piense antes de hacer clic!
¡No seas un phishing!
¡No esperes, actualiza!
Seguridad del navegador
No seas un acaparador
No compartas unidades
Conviértete en un detective de 30 segundos
Mantenga sus datos en secreto

Esta sección tiene como objetivo presentar o recordar a los participantes las mejores prácticas para mantener una buena higiene de seguridad digital.
Mejores prácticas de seguridad digital: introducción
Tener un software antivirus ejecutándose en tu dispositivo ofrece una capa adicional de seguridad; sin embargo, es fundamental reconocer que el software antivirus puede no siempre ser suficiente, especialmente ante ataques de día cero. Tu primera línea de defensa siempre debes ser tú mismo. Si sigue las mejores prácticas diarias y sencillas, podrá frustrar eficazmente a posibles adversarios.
{% embed url="https://www.youtube.com/watch?list=PL0Hcq8UCiYqlKxhKpH_K4P1m-HFSjdu7w&v=CztQyVYiXQs" %}

¡Sé un superhéroe cibernético! Por el Instituto de Acción del Tíbet
{% endeudado %}

1.  Contraseña segura


A menudo resulta tentador crear contraseñas simples y fáciles de memorizar para todas las plataformas a las que necesita acceder, simplemente porque hay muchas que recordar. Sin embargo, este enfoque puede conceder sin darse cuenta a adversarios y ciberdelincuentes un fácil acceso a todas sus cuentas.

Evite el uso de contraseñas que se puedan adivinar fácilmente, como su nombre, fecha de nacimiento, fechas importantes, nombres de familiares, lugar de nacimiento, etc.
En cambio, las contraseñas deben ser complejas e incorporar una combinación de letras mayúsculas y minúsculas, números y símbolos. El uso de varios caracteres hace que sea mucho más difícil adivinar para cualquiera.

A continuación se muestra un ejemplo de cómo crear una contraseña compleja pero fácil de recordar::

Construye una oración que sea fácil de recordar. Por ejemplo: "¡Mi lista de actividades favoritas definitivamente no implica correr a las seis de la mañana!"
Utilice las iniciales de cada palabra en la oración, incluidas las letras mayúsculas, y reemplace las palabras con números o símbolos cuando sea posible. Por ejemplo: "¡Mlofaddij@6itm!"




Recordar numerosas frases para diferentes cuentas puede resultar un desafío. Para simplificar este proceso, considera usar administradores de contraseñas como Bitwarden, que cifra completamente los datos, asegurando que ni siquiera los creadores de la herramienta puedan acceder a sus datos. Con un administrador de contraseñas, sólo necesita recordar una contraseña maestra para acceder a Bitwarden, donde puede almacenar de forma segura las contraseñas de todas sus cuentas.


2. ¡Haz los 2 pasos!


Ahora que has reforzado tu contraseña, has reforzado la seguridad de tu cuenta. Sin embargo, los atacantes aún pueden encontrar otras vías para obtener acceso no autorizado. Para agregar una capa adicional de protección, considera habilitar la autenticación o verificación en dos pasos si la plataforma ofrece esta función. En lugar de depender de la verificación por SMS, que puede ser interceptada, opta por una aplicación de autenticación confiable. Al hacerlo, incluso si un atacante logra descifrar tu contraseña, aún no podrá acceder a tu cuenta sin el segundo paso de verificación.

Además, si tu dispositivo corre riesgo de robo o confiscación, es esencial guardar códigos de respaldo. Estos códigos servirán como salvavidas y le permitirán recuperar el acceso a tu cuenta si pierde el acceso a tu aplicación de autenticación.


3.  ¡Sepárestede los archivos adjuntos!


{% embed url="https://www.youtube.com/watch?index=6&list=PL0Hcq8UCiYqn7PhgxpY2uiA4QkGVj_V1c&v=v4E1SRDmtZE" %}
¡Sepárese de los archivos adjuntos! Por el Instituto de Acción del Tíbet
{% endeudado %}
En el budismo, el apego se considera la raíz de todo sufrimiento, y este principio también se aplica a la seguridad digital. Los adversarios a menudo buscan acceso a tus sistemas para obtener control y manipular tus dispositivos y cuentas, obteniendo así acceso completo a tus datos. Una de las formas más populares que tienen los atacantes para lograr sus objetivos es incitar a sus objetivos a abrir archivos adjuntos maliciosos.
La mayoría de los ataques dirigidos están diseñados socialmente y a menudo aprovechan correos electrónicos o mensajes en plataformas como WhatsApp que contienen contenido muy relevante para ti o tu grupo. Estos mensajes explotan nuestros intereses y preocupaciones, haciéndolos más convincentes. Debemos saber que incluso en compromisos de alto perfil dentro del gobierno, las vulnerabilidades pueden surgir de errores individuales cometidos por los empleados, como descargar y abrir un archivo adjunto malicioso, a pesar de las sólidas medidas de ciberdefensa implementadas.
El malware podría proporcionar a intrusos acceso a tu red, archivos, cámara, micrófono, registro del teclado y otra información confidencial tan pronto como lo descargues y abras. Para evitar compromisos, es crucial desapegarse de los apegos. En lugar de enviar archivos adjuntos, priorice el uso de unidades cifradas para compartir, como Google Drive, Tresorit, etc., para compartir archivos de forma segura con \tus contactos o tu red.
Si recibes un archivo adjunto que parece genuino, absténte de descargarlo y abrirlo inmediatamente. En su lugar, verifica su legitimidad con el remitente a través de otra plataforma antes de realizar cualquier acción adicional.

4. ¡Piense antes de hacer clic!


Enviar enlaces maliciosos es otra táctica que utilizan los adversarios para atacarte. De manera similar a los archivos adjuntos maliciosos, los adversarios pueden crear mensajes o correos electrónicos personalizados para incitarlo a hacer clic en estos enlaces, comprometiendo potencialmente tu red, tus datos personales o la información de tu organización, o tomando el control de tu dispositivo. En los ataques dirigidos, es posible que los enlaces que se muestran en el mensaje, especialmente en los correos electrónicos, no conduzcan a los destinos que parecen. A continuación se ofrecen algunos consejos sobre cómo manejar este tipo de situaciones:

No hagas clic en enlaces en correos electrónicos o mensajes a menos que sea absolutamente necesario. Si te sientes obligado a abrir un enlace, verifica su legitimidad con el remitente a través de otra plataforma antes de hacer clic en él.
Si estás utilizando una computadora portátil o una PC, coloca el mouse sobre el enlace y verifica si el enlace que aparece coincide con el del correo electrónico. Ten en cuenta que los adversarios suelen crear vínculos muy similares a los de empresas, organizaciones o agencias de noticias legítimas para engañar a los destinatarios. Examina el enlace cuidadosamente para detectar cualquier discrepancia. Por ejemplo, si el enlace parece conducir a google.com, en realidad puede ser go0gle.com o goo9le.com, o google-com.com, etc.


5. ¡No seas victima de un  phishing!


{% embed url="https://www.youtube.com/watch?index=3&list=PL0Hcq8UCiYqkUVnpduzynZw9dpGRcqNhs&v=gNRzivSBonQ" %}
¡No seas phishing! Por el Instituto de Acción del Tíbet
{% endeudado %}

El phishing es otra táctica que los adversarios podrían emplear para robar su información personal, como contraseñas o datos de inicio de sesión bancarios. Los ataques de phishing pueden variar en escala, desde intentos dirigidos hasta campañas generalizadas, según el motivo.
En tales ataques, particularmente cuando están dirigidos a objetivos, los perpetradores pueden hacerse pasar por personas o grupos con los que usted tiene afiliaciones o en quienes naturalmente confiaría, como organizaciones asociadas o financiadores. También pueden hacerse pasar por organizaciones acreditadas como Google o bancos (los nombres varían según la región) y enviar correos electrónicos incitándolo a ingresar tu información personal en una página de inicio de sesión falsa.

Nunca ingreses tu nombre de usuario y contraseña si recibes un correo electrónico o mensaje solicitando dicha información.
Si bien las organizaciones, los bancos y los proveedores de servicios de correo electrónico pueden enviar notificaciones en caso de una infracción, los atacantes aprovechan esto para intentar engañarlo. Si no estás seguro de la legitimidad de un correo electrónico, comunícate directamente con la organización o el banco para confirmar su autenticidad.
Además, para tu correo electrónico y otras plataformas de comunicación, habilita la verificación en dos pasos como capa adicional de seguridad.


 ¡No esperes, actualiza!


A menudo, cuando las personas reciben avisos en sus computadoras que les solicitan que actualicen el software con opciones como "Actualizar ahora" o "Recordarme más tarde", pueden optar por posponer la actualización debido a que están ocupados en otras tareas. Sin embargo, las actualizaciones no sólo introducen funciones nuevas y mejoradas, sino que también proporcionan parches de seguridad para cualquier vulnerabilidad detectada en versiones anteriores.

Optar por 'Actualizar ahora' es una forma sencilla pero poderosa de mejorar la seguridad en línea. Esto se debe a que muchos ataques maliciosos aprovechan las vulnerabilidades que se encuentran en versiones anteriores de sistemas operativos, software y aplicaciones.


7. Seguridad del navegador


Los navegadores sirven como puerta de entrada a Internet, facilitando el acceso a una gran cantidad de información. Sin embargo, también almacenan cantidades importantes de datos personales, incluido el historial de navegación, las credenciales de usuario y la información bancaria. Además, el acceso a sitios maliciosos puede exponer a los usuarios a diversas amenazas, lo que sirve como punto de entrada para los atacantes. Por tanto, adoptar buenas prácticas en torno a la seguridad del navegador es fundamental para defenderse de posibles amenazas. Aquí hay algunos pasos que puede seguir para garantizar una navegación segura:
Elije el navegador adecuado:

Elija navegadores que prioricen la seguridad. Opciones como Chrome, Brave, Firefox y otras son conocidas por su enfoque en funciones de privacidad y seguridad.

Manten tu navegador actualizado

Actualizar periódicamente tu navegador es fundamental para la seguridad. Muchos ataques se dirigen a vulnerabilidades encontradas en versiones anteriores, por lo que mantenerse actualizado ayuda a mantener a raya a los atacantes.

Utiliza complementos/extensiones para mejorar la seguridad del navegador:

Los complementos como uBlock Origin, NoScript y Privacy Badger ayudan a bloquear scripts y rastreadores maliciosos, lo que reduce el riesgo de verse comprometido durante la navegación.
HTTPS Everywhere aplica el cifrado HTTPS siempre que sea posible, mejorando la privacidad y la seguridad siempre que sea posible y proporcionando privacidad durante la navegación.

Utiliza el navegador privado/de incógnito:

Cuando navegues por sitios web confidenciales o realices actividades privadas, utiliza el modo Incógnito o Navegación privada disponible en la mayoría de los navegadores. Este modo elimina automáticamente el historial de navegación y las cookies cuando se cierra la sesión, minimizando el riesgo de exposición.

Eliminar el historial del navegador:

Borra periódicamente tu historial de navegación para eliminar cualquier rastro de tu actividad en línea. Esto ayuda a proteger tu privacidad y evita el acceso no autorizado a sus hábitos de navegación.

No guardes información personal:

Evita guardar credenciales de inicio de sesión, datos bancarios u otra información confidencial en tu navegador. Si bien puede parecer conveniente, almacenar esta información hace que sea fácilmente accesible para cualquier persona que acceda a tu dispositivo.


8.- No seas un acaparador


{% embed url="https://www.youtube.com/watch?index=1&list=PL0Hcq8UCiYqkUVnpduzynZw9dpGRcqNhs&v=xLMH4DK-Y1I" %}
¡No seas acaparador! Por el Instituto de Acción del Tíbet
{% endeudado %}

En nuestro acelerado mundo digital, es fácil acumular un montón de información personal y relacionada con el trabajo en nuestros armarios digitales. Estos pueden incluir correos electrónicos, conversaciones de chat, fotografías y archivos, lo que plantea riesgos potenciales no solo para nosotros mismos sino también para nuestras familias y redes, dependiendo de la sensibilidad del contenido. Si bien es difícil encontrar tiempo para la limpieza digital en medio de nuestras apretadas agendas, dedicar un tiempo específico anualmente a la limpieza profunda puede ser inmensamente beneficioso. No sólo nos protege contra futuras amenazas, sino que también mejora el rendimiento del dispositivo. A continuación se ofrecen algunos consejos para evitar el acaparamiento digital:

Elimine periódicamente las comunicaciones o chats confidenciales de ambos extremos una vez concluida la conversación.
Elimine los medios y otros archivos inmediatamente después de su uso si ya no sirven para ningún otro propósito.


9. No compartas unidades


Las memorias USB pueden parecer reliquias para muchos, pero los dispositivos de almacenamiento externos siguen siendo cruciales para la copia de seguridad de datos y el almacenamiento fuera de línea. Sin embargo, es importante tener precaución al utilizar estas unidades. Evita compartirlos o conectarlos a dispositivos que no sean los tuyos. Si el otro dispositivo está comprometido, podría representar un riesgo para los datos almacenados en tu disco, así como para su computadora portátil o PC.

Utilice plataformas para compartir archivos como Google Drive, Tresorit o servicios similares para compartir unidades en línea de forma segura.
Si no es posible compartir en línea, considere usar funciones como AirDrop, Bluetooth u otras opciones de uso compartido cercanas para transferir datos de forma segura entre dispositivos.


10.  Conviértete en un detective de 30 segundos


En el ámbito de la comunicación digital, al igual que una carta tradicional, un correo electrónico contiene información crucial sobre su origen y destino. Incluso cuando el remitente sea familiar, es imperativo verificar la autenticidad de los correos electrónicos que contienen enlaces o archivos adjuntos. Dedique al menos 30 segundos a examinar posibles señales de alerta, que incluyen:

Discrepancias ortográficas en el nombre del remitente (por ejemplo, "Callvin" en lugar de "Calvin").
Los remitentes reconocibles que utilizan dominios desconocidos (por ejemplo, "calvin@yahoo.com" o "calvin@gmaiil.com" en lugar de "calvin@gmail.com") pueden ser una señal de alerta para posibles intentos de phishing.
Presencia de enlaces o archivos adjuntos dentro del correo electrónico.
Contenido genérico o impersonal en el cuerpo del correo electrónico.

Si permaneces alerta e identifics estas señales de advertencia, podrás protegerte contra posibles intentos de phishing o actividades fraudulentas.

11.Manten sus datos en secreto


Si resides en un área con censura y vigilancia significativas, si estás bajo monitoreo activo, es crucial incorporar Tor o VPN confiables o acreditadas en tus prácticas de seguridad digital siempre que sea posible. Esto es especialmente importante al transmitir o acceder a información confidencial.

VPN: una VPN buena o confiable proporcionará:

Privacidad
Accesibilidad
Seguridad
Ej.: Psiphon, Linterna


Tor proporciona:

Privacidad
Accesibilidad
Seguridad
Anonimato
Ej. Orbot
