
¡Vivir mejor mediante el modelado de amenazas!

Explicación:
Normalmente, llevamos a cabo una capacitación sobre modelado de amenazas al principio, lo que permite a los participantes actualizar continuamente sus Activos, Riesgos, Vulnerabilidades y Mitigación a medida que los reconocen a lo largo de la capacitación. Este enfoque es adecuado para sesiones de entrenamiento de 2 a 3 días. Sin embargo, para sesiones más cortas, hemos descubierto que realizar un modelado de amenazas al final, después de que los participantes hayan comprendido todos los pasos de mitigación, resulta más efectivo.


A medida que exploramos prácticas de seguridad digital para protegernos contra posibles amenazas cibernéticas, es esencial reconocer que la ciberseguridad es altamente individualizada. Lo que puede funcionar para una persona puede no serlo para otras, dadas sus circunstancias únicas. Entran en juego factores como los objetivos personales, los riesgos asociados y las estrategias de mitigación.
En el panorama en constante evolución de la seguridad digital, las actualizaciones continuas y la adaptabilidad son cruciales. Para facilitar esta comprensión, profundizaremos en una versión condensada del ejercicio Modelado de amenazas. Este ejercicio sirve como un marco valioso para evaluar riesgos e implementar medidas de protección.
Instrucción:

Divide a los participantes en 3 grupos y asigna a cada grupo un escenario. Agregua más escenarios (y grupos) según la cantidad de participantes o las necesidades y experiencias regionales.
Durante el ejercicio, cada grupo abordará su escenario asignado y trabajará juntos para idear soluciones basadas en el contenido cubierto en los módulos 1, 2 y 3. También se anima a los participantes a aprovechar sus propias experiencias e incluir soluciones adicionales no cubiertas en los módulos. . Se deben tener en cuenta las experiencias de la vida real al formular soluciones. Las respuestas a las siguientes preguntas sentarán las bases para sus modelos de amenazas individuales, dando forma a su enfoque de la seguridad digital:

¿Qué activos pretendo o pretendemos proteger?
¿Quién plantea amenazas potenciales a estos activos?
¿Cuáles son las consecuencias de una violación de seguridad?
¿Qué posibilidades hay de que necesitemos proteger estos activos?
¿Qué medidas tomaríamos yo/nosotros y yo/nuestros colegas para proteger estos activos?



Sugerencia:
Los escenarios para este ejercicio pueden basarse en las experiencias y conocimientos de la vida real de los participantes y su región. Aprovechar escenarios reales ayudará a generar estrategias de mitigación más auténticas.
Escenario 1:
Tuy tu equipo de defensores están organizando un importante evento público dentro de cinco meses para protestar contra una política gubernamental que socava los derechos humanos básicos. Anticipando los incesantes esfuerzos del gobierno para impedir la organización de este evento, es imperativo una mayor vigilancia y planificación estratégica.
En una semana, tendrá su reunión inaugural con todo el equipo organizador, compuesto por defensores y socios con distintos niveles de riesgo: bajo, medio y alto. Durante esta sesión, discutirán información confidencial y tomarán decisiones fundamentales con respecto a la planificación de eventos y stu equipo de organizadores tendrá que continuar la comunicación durante los próximos cinco meses.

Escenario 2:
Ya han pasado 5 meses y solo quedan dos días para el gran evento. ¡Felicidades por haber llegado tan lejos! A pesar de haber enfrentado intimidación por parte de las autoridades durante los últimos cinco meses, tu y tu equipo han perseverado. Sin embargo, hay noticias preocupantes: se ha corrido la voz de que el gobierno planea traer fuerzas policiales de los estados o provincias vecinos como respaldo y tiene la intención de reprimir el evento. Como organizador que participa en el evento, existe una alta probabilidad de ser arrestado antes o durante el evento.

Escenario 3:
Estás colaborando con una comunidad muy atacada en [Nombre de la región remota], que se resiste a la minería apoyada por el gobierno local. Las autoridades locales y la empresa minera intimidan persistentemente a los líderes comunitarios y amenazan con hacerles daño o arrestarlos si se les descubre conspirando contra el gobierno.
Su función implica ayudar a la comunidad a recopilar todas las pruebas relacionadas con la operación minera, que podrían incluir pruebas de corrupción, contaminación, efectos adversos sobre la salud y el bienestar de la población local y la vida silvestre, y otras cuestiones pertinentes. Sin embargo, sólo puedes visitar la región una vez cada tres meses y la mayor parte de tu comunicación con la comunidad se produce a través de tu teléfono móvil.
