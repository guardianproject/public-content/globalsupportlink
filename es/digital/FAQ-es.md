
Preguntas Frecuentes
---------------------
---------------------
**¿Cómo sé si mi dispositivo está comprometido?**

Aquí hay algunas señales que pueden indicar que algo anda mal con tu dispositivo:

1. Problemas de batería y rendimiento: agotamiento repentino y rápido de la vida útil de la batería, calentamiento inusual del dispositivo y disminución notable del rendimiento del dispositivo o apagados repentinos
2. Archivos perdidos: archivos que desaparecen misteriosamente.
3. Ventanas emergentes inesperadas: ventanas y pantallas inesperadas que aparecen de pronto.
4. Instalaciones no autorizadas: aplicaciones o software no autorizados instalados en tu dispositivo.
5. Actividad sospechosa de la cuenta: tus cuentas de correo electrónico o redes sociales inician sesión desde diferentes ubicaciones.
6. Cámara y micrófono activos: la cámara y el micrófono de tu dispositivo están activos cuando no los estát usando.


Es posible que algunos dispositivos no muestren signos evidentes de compromiso. Si sospechas que tu dispositivo está comprometido y no tienes los recursos para eliminar el malware, el mejor curso de acción es hacer una copia de seguridad de tus archivos y realizar un restablecimiento de fábrica.

**¿Cómo sé si mi correo electrónico está comprometido?**

Aquí hay algunas señales que pueden indicar que tu correo electrónico puede estar comprometido:

1. No se puede iniciar sesión: no puedes iniciar sesión en tu cuenta de correo electrónico con tus credenciales.
2. Correos electrónicos enviados inusuales: Tus amigos comienzan a recibir correos electrónicos aleatorios tuyos o tu carpeta de enviados contiene correos electrónicos que nunca enviaste.
3. Notificaciones no autorizadas: comienzas a recibir notificaciones de intentos de inicio de sesión o cambios de contraseña que tu no hiciste.
4. Detalles de la cuenta modificados: los detalles de tu cuenta han cambiado, incluido tu número de teléfono, correo electrónico de recuperación, dirección de reenvío de correo, etc.
5. Actividad sospechosa de la cuenta: la actividad de tu cuenta muestra ubicaciones de direcciones IP no relacionadas contigo, inicios de sesión que ocurren en horas inusuales o dispositivos y navegadores que normalmente no usas.

Si su correo electrónico presenta alguno de estos signos:
1. Cambia inmediatamente tu contraseña.
2. Desde la actividad de tu cuenta, cierra sesión en dispositivos/ubicaciones que no te resulten familiares.
3. Configura la verificación en dos pasos.
4. Informa a todos los contactos que recibieron correos electrónicos maliciosos de tu cuenta.

Para evitar verse comprometido:
1. Utiliza contraseñas seguras
2. Abstenerse de descargar y abrir archivos adjuntos
3. Los proveedores de servicios de correo electrónico nunca envían correos electrónicos pidiéndote que ingrese tu nombre de usuario y contraseña en formularios integrados en un correo electrónico o mediante enlaces. Si recibes un correo electrónico de este tipo, no introduzcas tus credenciales. Esto podría ser un ataque de phishing. Si realmente necesita restablecer tu contraseña, debes solicitarlo a través del sitio web oficial donde recibirás un enlace para restablecerla de forma segura.
4. Habilita siempre la verificación en dos pasos para mayor seguridad
5. Actualiza periódicamente tu aplicación de correo electrónico, tu navegador y el sistema operativo (SO) de tu dispositivo para corregir las vulnerabilidades de seguridad.
6. Verifica la dirección de correo electrónico del remitente si un correo electrónico parece sospechoso y contiene archivos adjuntos o enlaces.


**¿Cómo sé si mi cuenta de redes sociales está comprometida?**

Aquí hay algunas señales que pueden indicar que tus redes sociales pueden estar comprometidas:
1. No se puede iniciar sesión: no puede iniciar sesión en su cuenta de correo electrónico con tus credenciales.
2. Publicaciones inusuales: comienza a aparecer contenido aleatorio en tu cuenta que tu no publicaste.
3. Mensajes inusuales: Tus amigos/contactos comienzan a recibir mensajes, enlaces y archivos adjuntos que no enviaste.
4. Notificaciones no autorizadas: comienzas a recibir notificaciones de intentos de inicio de sesión o cambios de contraseña que tu no solicitaste.
5. Detalles de la cuenta modificados: los detalles de tu cuenta han cambiado, incluidos los detalles de su biografía, foto de perfil, número de teléfono conectado con la cuenta, correo electrónico de recuperación, cuentas vinculadas, etc.
6. Actividad sospechosa de la cuenta: la actividad de tu cuenta muestra ubicaciones de direcciones IP no relacionadas contigo, inicios de sesión que ocurren en horas inusuales o dispositivos y navegadores que normalmente no usas.

Si tu cuenta presenta alguno de estos signos:
1. Cambia inmediatamente tu contraseña.
2. Cierra sesión en dispositivos/ubicaciones que no te resulten familiares.
3. Configura la verificación en dos pasos.
4. Informa a todos los contactos que recibieron correos electrónicos maliciosos de tu cuenta.

Para evitar verse comprometido:
1. Utiliza contraseñas seguras
2. Abstenerse de descargar y abrir archivos adjuntos
3. Evita hacer clic en enlaces sospechosos en publicaciones y mensajes.
4. Los proveedores de servicios de plataforma nunca envían correos electrónicos pidiéndote que ingrese tu nombre de usuario y contraseña en formularios integrados en un correo electrónico o mediante enlaces. Si recibes un correo electrónico de este tipo, no introduzcas tus credenciales. Esto podría ser un ataque de phishing. Si realmente necesita restablecer tu contraseña, debes solicitarlo a través del sitio web oficial donde recibirás un enlace para restablecerla de forma segura.
5. Habilite siempre la verificación en dos pasos para mayor seguridad
6. Actualiza periódicamente tu aplicación de redes sociales, tu navegador y el sistema operativo (SO) de tu dispositivo para corregir las vulnerabilidades de seguridad.

**¿Cómo sé si mi teléfono está siendo intervenido?**

El alcance de los dispositivos intervenidos se ha ampliado significativamente más allá de los métodos tradicionales y ahora incluye la vigilancia a través de las aplicaciones de comunicación que tu utilizas.

Los signos más comunes de haber sido intervenido durante llamadas telefónicas regulares:
Ruidos de fondo como clics, ecos o comentarios de voz.
Interrupciones en las llamadas, como caídas inesperadas cuando ninguna de las partes cuelga

Las llamadas telefónicas y los mensajes habituales pueden interceptarse mediante equipos de vigilancia.
Si tu dispositivo de comunicación está comprometido, también puede ser monitoreado cuando uses aplicaciones de comunicación. Aquí hay algunas señales que pueden indicar que algo anda mal con tu dispositivo:

1. Agotamiento repentino y rápido de la duración de la batería, calentamiento inusual del dispositivo y disminución notable del rendimiento del dispositivo o apagados repentinos
2. Archivos que desaparecen misteriosamente.
3. Aparecen ventanas y pantallas inesperadas.
4. Aplicaciones o software no autorizados instalados en tu dispositivo
5. tus cuentas de correo electrónico o redes sociales iniciando sesión desde diferentes ubicaciones.
6. La cámara y el micrófono de tu dispositivo están activos cuando no los está usando.
7. Es posible que algunos dispositivos no muestren signos evidentes de compromiso. Si sospechas que tu dispositivo está comprometido y no tiene los recursos para eliminar el malware, el mejor curso de acción es hacer una copia de seguridad de tus archivos y realizar un restablecimiento de fábrica.
Para evitar ser intervenido:
Utiliza siempre aplicaciones de comunicación que proporcionen cifrado de punto a punto (E2EE) como Signal y WhatsApp.
Sigue las mejores prácticas de seguridad digital para mantener tu dispositivo y tu cuenta seguros https://guide.globalsupport.link/digital/you-are-the-best-antivirus

**¿Cómo puedo crear una contraseña segura?**

A menudo resulta tentador crear contraseñas simples y fáciles de memorizar para todas las plataformas a las que necesita acceder, simplemente porque hay muchas que recordar. Sin embargo, este enfoque puede conceder sin darse cuenta a adversarios y ciberdelincuentes un fácil acceso a todas tus cuentas.
Evita el uso de contraseñas que se puedan adivinar fácilmente, como tu nombre, fecha de nacimiento, fechas importantes, nombres de familiares, lugar de nacimiento, etc.
En cambio, las contraseñas deben ser complejas e incorporar una combinación de letras mayúsculas y minúsculas, números y símbolos. El uso de varios caracteres hace que sea mucho más difícil adivinar para cualquiera.
A continuación se muestra un ejemplo de cómo crear una contraseña compleja pero fácil de recordar::
Construya una oración que sea fácil de recordar. Por ejemplo: "¡Mi lista de actividades favoritas definitivamente no implica correr a las seis de la mañana!"
Utilice las iniciales de cada palabra en la oración, incluidas las letras mayúsculas, y reemplace las palabras con números o símbolos cuando sea posible. Por ejemplo: "¡Mlofaddij@6itm!"
Recordar numerosas frases para diferentes relatos puede resultar un desafío. Para simplificar este proceso, considere usar administradores de contraseñas como Bitwarden, que cifra completamente los datos, asegurando que ni siquiera los creadores de la herramienta puedan acceder a tus datos. Con un administrador de contraseñas, sólo necesita recordar una contraseña maestra para acceder a Bitwarden, donde puede almacenar de forma segura las contraseñas de todas tus cuentas.

**¿Es posible tener una comunicación segura?**

Sí, la comunicación segura es posible con el cifrado de punto a punto (E2EE). Para obtener más información, visita: https://guide.globalsupport.link/digital/safeguard-your-conversation-and-shared-secrets

¿Es seguro mantener una conversación a través de SMS y llamadas telefónicas habituales?
Las llamadas telefónicas y los mensajes habituales pueden interceptarse mediante equipos de vigilancia.

Para una comunicación segura:
Utiliza siempre aplicaciones de comunicación que proporcionen cifrado de punto a punto (E2EE) como Signal y WhatsApp.
Sigue las mejores prácticas de seguridad digital para mantener tu dispositivo y tu cuenta seguros https://guide.globalsupport.link/digital/you-are-the-best-antivirus


**¿Cuál es la mejor aplicación de mensajería segura?**

Hay algunos criterios que debemos considerar al elegir una aplicación segura.
* Si la aplicación proporciona cifrado de punto a punto (E2EE)
* Si la aplicación garantiza el anonimato
* Si el desarrollador de la aplicación o el proveedor de servicios tiene un historial de asociación o trabajo con agencias que no te apoyan a ti ni a tu trabajo.

Para obtener una lista de aplicaciones de mensajería, consulte el cuadro "Chat seguro" en esta página: https://guide.globalsupport.link/digital/safeguard-your-conversation-and-shared-secrets

**¿Cómo puedo proteger mi correo electrónico?**

Aquí hay algunos pasos que puedes seguir para proteger tu correo electrónico:
1. Utiliza contraseñas seguras
2. Abstenerse de enviar archivos adjuntos o descargar y abrir archivos adjuntos recibidos
3. Los proveedores de servicios de correo electrónico nunca envían correos electrónicos pidiéndole que ingrese su nombre de usuario y contraseña en formularios integrados en un correo electrónico o mediante enlaces. Si recibes un correo electrónico de este tipo, no introduzcas tus credenciales. Esto podría ser un ataque de phishing. Si realmente necesita restablecer su contraseña, debe solicitarlo a través del sitio web oficial donde recibirá un enlace para restablecerla de forma segura.
4. Habilita siempre la verificación en dos pasos para mayor seguridad
5. Actualiza periódicamente su aplicación de correo electrónico, su navegador y el sistema operativo (SO) de su dispositivo para corregir las vulnerabilidades de seguridad.
6. Verifique la dirección de correo electrónico del remitente si un correo electrónico parece sospechoso y contiene archivos adjuntos o enlaces.

Implemente las mejores prácticas de seguridad digital descritas en esta página: https://guide.globalsupport.link/digital/you-are-the-best-antivirus


**¿La VPN oculta toda mi información personal y de navegación?**

Aunque las VPN tienen la capacidad de cifrar la información personal y de navegación de los usuarios cuando se conectan a Internet, las VPN no brindan un anonimato completo. Si deseas tener total anonimato al acceder a Internet, la mejor herramienta para eludirlo es Tor: https://www.torproject.org/

**¿Qué navegador es el más seguro?**

Elija navegadores que no estén creados ni asociados con agencias que no te apoyen a ti ni a tu trabajo. Estos son algunos de los navegadores seguros más populares: Tor Browser, Brave, Firefox, Chrome y Safari.

Proteger tu experiencia de navegación también requiere implementar las mejores prácticas de seguridad del navegador:

Elije el navegador adecuado:
* Elije navegadores que prioricen la seguridad. Opciones como Chrome, Brave, Firefox y otras son conocidas por su enfoque en funciones de privacidad y seguridad.
* Manten tu navegador actualizado
* Actualizar periódicamente tu navegador es fundamental para la seguridad. Muchos ataques se dirigen a vulnerabilidades encontradas en versiones anteriores, por lo que mantenerse actualizado ayuda a mantener a raya a los atacantes.
* Utiliza complementos/extensiones para mejorar la seguridad del navegador:
* Los complementos como uBlock Origin, NoScript y Privacy Badger ayudan a bloquear scripts y rastreadores maliciosos, lo que reduce el riesgo de verse comprometido durante la navegación.
* HTTPS Everywhere aplica el cifrado HTTPS siempre que sea posible, mejorando la privacidad y la seguridad siempre que sea posible y brinda privacidad mientras navegas
* Utiliza el navegador privado/de incógnito:
* Cuando navegues por sitios web confidenciales o realices actividades privadas, utiliza el modo Incógnito o Navegación privada disponible en la mayoría de los navegadores. Este modo elimina automáticamente el historial de navegación y las cookies cuando se cierra la sesión, minimizando el riesgo de exposición.
* Eliminar el historial del navegador:
* Borra periódicamente su historial de navegación para eliminar cualquier rastro de tu actividad en línea. Esto ayuda a proteger tu privacidad y evita el acceso no autorizado a tus hábitos de navegación.
* No guardes información personal:
Evite guardar credenciales de inicio de sesión, datos bancarios u otra información confidencial en tu navegador. Si bien puede parecer conveniente, almacenar esta información hace que sea fácilmente accesible para cualquier persona que acceda a tu dispositivo.

**¿Cómo protejo mi computadora portátil?**

Estos son los pasos que puedes seguir para proteger tu computadora portátil:
1. Bloquea tu computadora portátil usando contraseñas o frases de contraseña seguras. Recuerda, si bien Face ID, biometría, patrones simples y PIN de 4 dígitos son convenientes, no son las opciones más seguras. Estos métodos se pueden descifrar fácilmente o es posible que te obliguen a desbloquear el dispositivo, especialmente si vives bajo regímenes represivos.
2. Cifra el disco duro de tu dispositivo utilizando las herramientas disponibles en tu computadora portátil. Para computadoras portátiles con Windows, puede usar BitLocker y para macOS, puede usar FileVault. Esto ayudará a proteger los datos de tu dispositivo.
3. No utilices WiFi público (en aeropuertos, bibliotecas, cafeterías, etc.) a menos que sea absolutamente necesario. Si debes utilizar una WiFi pública, asegúrete de utilizar Tor o una VPN para conectarse a Internet.
4. Actualiza tu sistema operativo (SO) y tus aplicaciones con regularidad. No demores las actualizaciones.
5. Proteje tu dispositivo de los atacantes al separarlo con archivos adjuntos, pensar antes de hacer clic en los enlaces y evitar intentos de phishing. Si un correo electrónico o mensaje parece sospechoso, no lo abras. Si parece legítimo pero aún genera dudas, confírmalo con el remitente a través de otro canal.
6. Recuerda, eres el mejor antivirus. Implementa las mejores prácticas de seguridad digital descritas aquí para proteger su dispositivo y sus cuentas: https://guide.globalsupport.link/digital/you-are-the-best-antivirus.
Considera también la seguridad física de tu dispositivo. No lo dejes desatendido, especialmente en espacios públicos.


**¿Cómo protejo mi teléfono?**

Estos son los pasos que puede seguir para proteger su teléfono:
1. No permitas que nadie entre a tu dispositivo. Utiliza una frase de contraseña para desbloquear tu teléfono. Recuerda, si bien Face ID, biometría, patrones simples y PIN de 4 dígitos son convenientes, no son las opciones más seguras. Estos métodos se pueden descifrar fácilmente o es posible que te obliguen a desbloquear el dispositivo, especialmente si vives bajo regímenes represivos.
2. Cifra tus dispositivos móviles. Asegúrete de que el cifrado esté habilitado en tus dispositivos Android o iOS. Esto ayudará a proteger los datos de tu dispositivo.
3. No permitas que aplicaciones malas arruinen tu fiesta. Instale sólo las aplicaciones que necesites, ya que más aplicaciones aumentan el potencial de vigilancia. Asegúrate de que las aplicaciones que utilizas solo accedan a los datos y funciones del dispositivo necesarios para que funcionen correctamente.
4. No utilices WiFi público (en aeropuertos, bibliotecas, cafeterías, etc.) a menos que sea absolutamente necesario. Si debes utilizar una WiFi pública, asegúrese de utilizar Tor o una VPN para conectarse a Internet.
5. Actualiza tu sistema operativo (SO) y tus aplicaciones con regularidad. No demores las actualizaciones.
6. Proteje tu dispositivo de los atacantes alejandolo de los archivos adjuntos, pensar antes de hacer clic en los enlaces y evitar intentos de phishing. Si un correo electrónico o mensaje parece sospechoso, no participes. Si parece legítimo pero aún genera dudas, confírmalo con el remitente a través de otro canal.
7. Habilite "Buscar mi teléfono" para localizar tu dispositivo si se pierde o se extravía. Algunas funciones de Find My Phone pueden resultar valiosas para los defensores, especialmente cuando tu dispositivo es confiscado o cae en manos de adversarios. 

**Así es como puedes habilitar Buscar mi teléfono:**

iPhone: https://support.apple.com/en-ca/102648
Android: https://support.google.com/accounts/answer/3265955?hl=en
Cuando tu dispositivo sea confiscado o esté en posesión de adversarios, considera las siguientes acciones según tu situación:
1. Inicia sesión en android.com/find o icloud.com/find según el dispositivo que estés utilizando.
2. Configura de forma remota un número PIN de dispositivo si aún no lo has hecho. Esto puede retrasar el acceso de otras personas a tu dispositivo.
3. Borra de forma remota los datos de tu dispositivo si tiene alguna información que pueda comprometer tu seguridad y la de tu red.
4. Recuerda, eres el mejor antivirus. Implemente las mejores prácticas de seguridad digital descritas aquí para proteger tu dispositivo y sus cuentas: https://guide.globalsupport.link/digital/you-are-the-best-antivirus
5. Considere también la seguridad física de su dispositivo. No lo dejes desatendido, especialmente en espacios públicos.

**¿Qué debo hacer para evitar que las aplicaciones rastreen mi ubicación?**

Estos son los pasos que puede seguir para evitar que las aplicaciones rastreen tu ubicación:
1. Desactiva el servicio de ubicación en todo momento. Habilítalos solo cuando necesites utilizar una función basada en la ubicación.
2. No permitas que las aplicaciones accedan a tu ubicación. Solo conceda acceso a la ubicación cuando sea necesario para que la aplicación funcione correctamente. Vaya a Configuración del dispositivo y revoque los permisos desde Permisos de aplicaciones (para Android) y Privacidad (para iOS).
3. Restrinja las aplicaciones, especialmente las de redes sociales, para que no rastreen tu ubicación y recopilen datos. La mayoría de las aplicaciones tienen opciones para limitar el acceso dentro de la configuración de privacidad de la aplicación.
4. Restringe las aplicaciones de Personalización de anuncios o Publicidad de Apple a través de la Configuración de privacidad de tu dispositivo.
5. Utilice Tor o VPN para ocultar tu ubicación real.