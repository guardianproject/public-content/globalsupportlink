# ☎️ ¡Pide Ayuda!

☎️ ¡Pide Ayuda! Este servicio aún se está implementando por completo. Actualmente, podemos brindar asesoría y soporte técnico en temas relacionados con la seguridad física y digital y la resiliencia psicosocial. En algunos casos, podemos conectarlo con un socio u organización regional que esté en mejores condiciones para ayudarlo. Nota: No estamos en condiciones de brindar apoyo legal extenso y actualmente no llevamos a cabo actividades de promoción.

Chat público de la mesa de ayuda: chat.globalsupport.link

Soporte técnico:

Correo electrónico: ayuda@globalsupport.link

Signal o WhatsApp: +447886176827

Telegrama: @GlobalSupportLink\_bot

Esta mesa de ayuda funciona con software gratuito, de código abierto, seguro y auditado, y está alojada en un centro de datos controlado físicamente, centrado en la privacidad y preocupado por el medio ambiente.
