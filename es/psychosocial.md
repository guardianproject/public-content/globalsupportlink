

Si eres practicante de salud mental, defensor, estudiante, makiisa y únete a nuestra lista de correo para asegurarte de recibir nuestras invitaciones para los próximos talleres y diálogos gratuitos este 2024: https://bit.ly/MentalHealthSocialJustice
Cada taller/diálogo contará con un defensor u organización de la justicia social, que compartirá historias de experiencias vividas.
Regístrate si tienes curiosidad sobre:

¿Talleres de salud mental que exploran la colonización y la narcocultura a cargo de practicantes ecofeministas, queer afirmativos, colombianos, sudaneses y tal vez musulmanes?
¿La psicología popular filipina también?
¿Talleres sobre asesoramiento entre pares y apoyo a seres queridos y amigos afectados por desapariciones forzadas, etiquetas rojas y más?
¿Conexiones entre inflación, soberanía alimentaria, migración, derechos laborales, transporte, veganismo, militarización, clima y salud mental?