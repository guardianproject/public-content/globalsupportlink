---
descripción: ¡Impulsado por la gente y listo para ayudar!
---

# Bienvenido a tu Servicio de Asistencia

La Mesa de ayuda proporciona **recursos gratuitos** a las personas y
organizaciones que forman parte de la red mundial **Impulsado por personas
(PxP)**. Además de los recursos en línea, los miembros del [Help
Desk](ask-for-help.md) pueden responder a preguntas y proporcionar asesoramiento
sobre seguridad digital y física a través de los canales de chat, mensajería o
correo electrónico. En función de sus necesidades, la Mesa de ayuda también
puede organizar:

* Talleres y cursos presenciales sobre seguridad y resistencia para
  organizaciones de la sociedad civil
* Investigaciones sobre seguridad digital
* Apoyo de expertos en la elaboración de políticas de seguridad
* Reuniones informativas sobre seguridad en los viajes
* Apoyo psicosocial

**ComuníCATE hoy mismo para hablar con un miembro de la** [**PLATAFORMA DE
SOPORTE**](ask-for-help.md) para ver qué es lo mejor para ti o tu organización o
haz clic en los recursos disponibles en línea:

* [Seguridad Digital](digital/)
* [Seguridad física](physical/)
* [Apoyo psicosocial](psychosocial.md)

¡Este es un recurso en evolución que se actualizará y mejorará con los
comentarios de todos ustedes!
