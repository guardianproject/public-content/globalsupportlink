Seguridad física
Como defensores de los derechos humanos, a veces podemos encontrarnos en nuestro momento más vulnerable: viajar por terrenos desconocidos y, potencialmente, conocer gente nueva o hacer cosas que de otro modo no haríamos.
En esta sección, descubrirá algunas formas de anticipar los riesgos de seguridad, así como las medidas que puede implementar para prevenirlos o responder a ellos.
Recuerde, cada contexto tiene sus peligros específicos y cada persona tiene sus vulnerabilidades específicas; solo identificando ambos aspectos podremos determinar los riesgos de seguridad.



