Seguridad del hotel

Es importante asegurarse de que ee sientas seguro en el lugar donde te hospedarás. Tóma el tiempo para realizar la siguiente evaluación de seguridad al llegar a un nuevo lugar:

Verifica los puntos de acceso a la habitación (puertas, ventanas, salidas de incendios).
Examina la habitación, incluidos los gabinetes, los baños, las camas y las áreas de las ventanas en busca de cualquier cosa que parezca sospechosa.
Asegúrate de que el teléfono funcione correctamente llamando a la recepción.
Asegúreate de que sus colegas tengan la ubicación del hotel, el número de habitación y el número de teléfono.
Ten en cuenta la ruta de evacuación en caso de incendio o emergencia y utiliza las escaleras al menos una vez para familiarizarte con ellas.

Tenga en cuenta las siguientes mejores prácticas al alojarse en un hotel:

Siempre asegura las puertas cuando esté dentro de la habitación con candados y cadenas de seguridad.
Manten las cortinas de la habitación cerradas cuando afuera esté oscuro.
No abras la puerta a los visitantes (incluido el personal del hotel) a menos que sea posible identificarlos positivamente. Utiliza la mirilla o llama a la recepción para verificarlo.
Si está disponible, utiliza las cajas de seguridad del hotel para guardar dinero en efectivo, tarjetas de crédito, pasaporte y cualquier otro objeto de valor. No dejes objetos de valor ni documentos sensibles en la habitación.
Lleva una copia de su pasaporte y visa y deja los originales en el hotel o la oficina. Además, conserva una copia de tu pasaporte en tu país de origen.


Evaluación de seguridad y protección del hotel

A continuación encontrarás una lista de verificación que puede ayudarte a recordar qué buscar al registrarse en un nuevo hotel o evaluar uno antes de reservar una habitación. Si bien no todos los elementos de esta lista son siempre necesarios o están disponibles, es importante intentar marcar tantas casillas como sea posible. Podría ser una señal de alerta si notas que el hotel cuenta con muy pocas medidas de seguridad y, por lo tanto, es importante hablar con tus colegas o con tu superior jerárquico.


Seguridad contra incendios
☐ Plan/mapa de emergencia contra incendios
☐ Detectores de humo
☐ Aspersores
☐ Puertas de salida de emergencia
☐ Sistema de alarma contra incendios
☐ Extintores
☐ Escaleras de emergencia
☐ Múltiples salidas/entradas\
Sistemas y equipos de seguridad.
☐ Sistema de vigilancia CCTV
☐ Estacionamiento controlado
☐ Acceso controlado con llave/insignia
Ambiente
☐ Acceso por carretera principal
☐ Barreras/vallas/portones
☐ Distancia suficiente (desde el perímetro hasta el hotel)
☐ No hay edificios gubernamentales o militares cercanos
☐ Área de baja criminalidad
Encendiendo
☐ Alumbrado de emergencia en zonas públicas y escaleras de evacuación
☐ Áreas de estacionamiento iluminadas
☐ Locales y terrenos iluminados
Perfil
☐ Hotel de bajo perfil, poco probable que sea un objetivo
☐ No muy frecuentado por viajeros internacionales, p. Trabajadores de ONG, turistas, etc. (En algunas áreas, los hoteles de cadenas occidentales de alto nivel con muchos turistas o extranjeros presentes podrían ser el objetivo de grupos extremistas. Sin embargo, estos hoteles a menudo tienen muy buenas medidas de seguridad protectora. Esta es una de las razones por las que es tan importante realizar un Análisis Rápido de Riesgos antes de viajar o reservar un hotel. Es vital sopesar los riesgos según el contexto).
custodiando
☐ Personal de seguridad en el lugar las 24 horas del día
☐ Patrullas de seguridad
☐ Acceso supervisado a entradas/salidas públicas
☐ Centro de mando con personal
☐ Guardias armados (sólo aconsejable en determinados contextos, de lo contrario puede llamar más la atención y crear más riesgo para el viajero)
Salud y Seguridad
☐ Botiquines de primeros auxilios/trauma
☐ Equipo DEA\Habitación de huéspedes
☐ Cerraduras de cerrojo
☐ Pestillo de cadena/horquilla de puerta
☐ Mirillas
☐ Mapas de salidas de seguridad
☐ Rejas/cerraduras en las ventanas
☐ Seguro
☐ Wi-Fi/acceso a Internet

