Consejos para mitigar los riesgos comunes

Vigilancia y Contravigilancia
Tus movimientos o actividades podrían ser monitoreados físicamente por un individuo o grupo hostil para determinar tus vulnerabilidades y/o planificar un ataque. Algunas señales de que estás bajo vigilancia física podrían incluir:

Extraños que hacen preguntas intrusivas sobre tus colegas o tu trabajo.
Ver al mismo extraño en varios lugares diferentes
Una cantidad sospechosa de llamadas de personas que afirman haber llamado a un número equivocado
Ser seguido, ya sea a pie o en un vehículo.
Vendedores, mendigos, merodeadores, etc. nuevos o inusuales, alrededor de tu casa u oficina

La observación de las siguientes precauciones puede aumentar la seguridad:

Evite rutinas predecibles como correr o ir de compras a la misma hora todos los días, especialmente cuando esté solo.
Viaja con otras personas y en grupos, y con socios locales de confianza en la medida de lo posible.
Cambia las rutas y los tiempos de viaje con regularidad
Utilice vehículos diferentes si es posible y modifique rutas, horarios y secuencias, compartiendo información según sea necesario.
Evite llamar la atención: no use relojes ni joyas costosos ni muestre grandes cantidades de dinero en efectivo; vestirse modestamente; no hables por teléfono ni uses auriculares mientras camina en público
Si sientes que estás bajo vigilancia, diríjase a un lugar seguro y bien iluminado donde haya otras personas presentes y busca ayuda.


Acoso sexual
El acoso sexual no se limita a las mujeres: hombres y mujeres de todas las edades, etnias y grupos económicos están en riesgo. Sin embargo, ciertos aspectos de tu perfil individual, como la identidad de género, la orientación sexual o el origen étnico, pueden hacerte más vulnerable al acoso sexual. El acoso y la agresión sexual no se denuncian; Las víctimas suelen ser atacadas con antelación y el agresor suele ser un conocido. Nunca es culpa tuya si te acosan sexualmente. La observación de las siguientes precauciones puede aumentar la seguridad, pero no son infalibles y el responsable final siempre recae en el perpetrador:

Escucha tus instintos: si algo no le parece bien, abandona la situación lo más rápido posible.
Los límites personales (privacidad, espacio personal, etc.) son diferentes en todas partes. Pregunta y siga las señales locales.
Vístete apropiadamente, respetando la cultura local.
Cuando se hospede en un hotel, trate de recibir a los huéspedes en el lobby o en un área neutral, no en tu habitación de hotel.
Mantenga la información personal privada. No divulgue información personal como el nombre del hotel, la dirección o el número de teléfono celular. Ofrezca la información de contacto de la persona en lugar de proporcionar la tuya propia.
Informa siempre cualquier comportamiento sospechoso o amenazante a tu organización o a alguien con quien te sientas seguro.


Disturbios civiles
Los disturbios civiles pueden ocurrir por diversas razones, la mayoría de las cuales están relacionadas con cuestiones sociales o políticas. Los países que atraviesan una agitación política generalizada pueden experimentar episodios sostenidos de malestar por motivos políticos que van desde pequeñas concentraciones no organizadas hasta manifestaciones y disturbios a gran escala, cualquiera de los cuales puede tornarse violentos sin previo aviso. Si bien es raro que los visitantes sean atacados directamente durante incidentes de disturbios, observar las siguientes precauciones puede aumentar la seguridad:

Monitorear noticias locales e internacionales.
Familiarícese con las condiciones locales. Averigüa si ha habido incidentes recientes de disturbios y, de ser así, su causa, gravedad y cómo respondieron las autoridades.
Evite las áreas donde se llevan a cabo manifestaciones o permanezca en el interior hasta que quede claro que la situación se ha estabilizado.
Salga del área y busque una ruta alternativa al destino previsto si se encuentra cerca de un área de disturbios.
No viajes solo a zonas donde existe una gran probabilidad de manifestaciones.

Si te encuentras entre la multitud:

Manténgase cerca y mantenga contacto visual con la persona con la que está.
Evita cualquier situación en la que la policía o las fuerzas de seguridad interactúen activamente con los manifestantes. Las autoridades de algunos países no toleran la disidencia y pueden utilizar fuerza excesiva en un esfuerzo por sofocar los disturbios.
Mantenga la calma, ya que es probable que las multitudes se disipen en un corto período de tiempo.
Manténgase al margen de la multitud y manténgase alejado de los líderes y agitadores. Trate de evitar ser identificado como uno de los manifestantes.
Cree espacio entre la multitud agarrando las muñecas y alejando los codos. Inclinarse ligeramente le permitirá respirar.
Manténgase alejado de las fachadas de vidrio de las tiendas y muévase con el flujo de la multitud.
Si lo empujan al suelo, intenta colocarte contra una pared, forme una bola apretada y proteje la cabeza con las manos hasta que la multitud se disipe.
Escapate y busca refugio en un edificio cercano a la primera oportunidad. Alternativamente, busque una entrada o callejón adecuado y permanezca allí hasta que pase la multitud.
Evite llamar la atención. Al abandonar el área de una manifestación, aléjese lentamente y evite la tentación de correr.
Si estalla un tiroteo, trate de encontrar refugio.
Si es arrestado por las fuerzas de seguridad, no se resista.


Transporte público
La alta densidad de personas que utilizan el transporte público crea condiciones ideales para la conducta delictiva, ya que aumenta el número de víctimas potenciales y proporciona anonimato al perpetrador. Los incidentes comunes pueden incluir delitos menores como robo o hurto, o incidentes más graves como el secuestro. La observación de las siguientes precauciones puede aumentar la seguridad:

Nunca hagas autostop ni aceptes que te lleven extraños
Evita viajar solo
Tenga lista la ficha o cambio adecuado cuando se acerque a la taquilla o máquina
Tenga cuidado con los carteristas y ladrones mientras espera el transporte.
Espera el autobús o el tren en áreas de espera designadas y bien iluminadas, especialmente durante las horas de menor actividad.
Abandona cualquier transporte público que te resulte incómodo o amenazante.
Después de bajar, verifique que nadie lo esta siguiendo.
Evita viajar en autobús o tren por la noche. Si esto es inevitable:

Evita autobuses o vagones de tren vacíos
Siéntate cerca del conductor en los autobuses o en el vagón del medio con otros pasajeros en los trenes.
Siéntate junto a una ventana o cerca de puertas, que permitan una salida rápida en caso de accidente.

Caminando

Utilice rutas bien transitadas y bien iluminadas y busque asesoramiento sobre áreas locales consideradas seguras para caminar.
Consulte un callejero local o Google Maps antes de partir.
Evita caminar solo o de noche.
Camina con confianza, pero manténte alerta ante posibles problemas.
No uses auriculares, ya que pueden reducir la conciencia situacional y pueden indicar riqueza.
Evita caminar demasiado cerca de arbustos, puertas oscuras y otros lugares donde los delincuentes puedan esconderse.
Ten cuidado con los empujones en áreas concurridas. Divide el dinero y las tarjetas de crédito entre dos o tres bolsillos o bolsos, ya que los carteristas suelen trabajar en parejas y utilizan la distracción como estrategia básica.
Manten la mochila o el bolso cerca del cuerpo para evitar robos y huidas. No lleves objetos de valor en estas bolsas; en su lugar, deja los objetos de valor en un lugar seguro.
Lleva sólo una pequeña cantidad de dinero y un reloj barato para entregárselo si te amenazan.
Si un conductor se acerca para pedir direcciones, no te acerques al vehículo.
Si alguien parece sospechoso, cruza la calle o cambia de dirección para alejarte de la persona. Si es necesario, cruza la calle varias veces. Si la persona te sigue o se convierte en una amenaza, utiliza todos los medios necesarios para atraer la atención de otro espectador. Gritando "¡Fuego!" A menudo atrae más atención que gritar "¡Ayuda!" Recuerde, es mejor avergonzarse por ser demasiado cauteloso que ser víctima de un delito.


Robo o intimidación

No intentes intimidar al agresor ni ser agresivo. En lugar de ello, se cortés, abierto y confiado; Trata de no mostrar enojo o miedo.
Habla con calma y claridad.
