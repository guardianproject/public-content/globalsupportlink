Seguridad del dispositivo: preparación para robo, pérdida o confiscación

Ya sea cruzando una frontera internacional o un puesto de control, asistiendo a una manifestación o caminando por una ciudad desconocida, nuestros dispositivos, especialmente nuestros teléfonos móviles, siempre corren el riesgo de ser robados, extraviados o confiscados.
Antes de viajar o asistir a un evento que podría provocar tu detención o arresto, haga una copia de seguridad de la mayor cantidad posible de tus dispositivos en el almacenamiento en la nube o en el servidor de tu organización, incluidas contraseñas, conversaciones de mensajería y documentos.

Documentos y archivos

Elimina todos los archivos confidenciales del dispositivo o muévelos a un volumen cifrado.
Borra todo el contenido de las carpetas Descargas, Imágenes, Música y Vídeo.
Vacía la papelera de reciclaje.
Software de cifrado y mensajería
Desinstale el software de cifrado y vuelva a instalarlo solo una vez que llegue al destino.
Una vez que se realiza una copia de seguridad de las conversaciones, debes cerrar sesión y desinstalar cualquier aplicación de mensajería confidencial.
Si necesitas conservar una aplicación de mensajería para fines de comunicación, configura los mensajes que desaparecen durante el menor tiempo posible.
Elimina cualquier aplicación de correo electrónico y confía en el correo web tanto como sea posible.
Navegadores web
Cierra sesión en todos los sitios web y servicios.
Borra archivos temporales de Internet y el historial de navegación.
Redes Privadas Virtuales (VPN)
Se deben borrar los registros y perfiles de VPN.
Archivos temporales y cachés de Windows
Has clic en Inicio y luego escribe "Liberador de espacio en disco" en la barra de búsqueda de programas y archivos.
Elija Unidad C: de la lista desplegable y presiona OK.
Marcae todas las opciones en el cuadro "Archivos para eliminar" y presione Aceptar.


